#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "app/person_data_form/types/BodyData.h"
#include "app/person_data_form/types/GeneralData.h"
#include "app/person_data_form/types/PersonData.h"

#include <QDoubleValidator>
#include <QMainWindow>

QT_BEGIN_NAMESPACE

namespace Ui {
class PersonFormWindow;
}  // namespace Ui

QT_END_NAMESPACE

class PersonFormWindow : public QMainWindow {
    Q_OBJECT

   public:
    PersonFormWindow(QWidget* parent = nullptr);
    ~PersonFormWindow();

   private slots:
    void on_pushButton_clicked();

    void on_nextPageButton_clicked();

    void on_prevPageButton_clicked();

   private:
    Ui::PersonFormWindow* ui;
    QDoubleValidator q_double_validator_;
    GeneralData* general_data_;
    BodyData* body_data_;
};
#endif  // MAINWINDOW_H
