//
// Created by savchik on 28/05/24.
//

#ifndef OUTFIT_MANAGER_GENERALDATAREPOSITORY_H
#define OUTFIT_MANAGER_GENERALDATAREPOSITORY_H

#include "core/database/abstract_repository.h"

#include "GeneralDataEntity.h"


class GeneralDataRepository : public outfit::core::database::AbstractRepository<GeneralDataEntity> {
    static QString TableName();
};


#endif //OUTFIT_MANAGER_GENERALDATAREPOSITORY_H
