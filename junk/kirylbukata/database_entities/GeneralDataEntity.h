//
// Created by savchik on 28/05/24.
//

#ifndef OUTFIT_MANAGER_GENERALDATAENTITY_H
#define OUTFIT_MANAGER_GENERALDATAENTITY_H

#include "core/database/abstract_entity.h"
#include "app/person_data_form/types/GeneralData.h"

#include <QSqlRecord>

class GeneralDataEntity : public outfit::core::database::AbstractEntity<int> {

   public:
    explicit GeneralDataEntity(const QSqlRecord &record);

    GeneralData& GetGeneralData();

   private:
    GeneralData general_data_{};
};


#endif //OUTFIT_MANAGER_GENERALDATAENTITY_H
