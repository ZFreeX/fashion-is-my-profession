#ifndef BODYDATA_H
#define BODYDATA_H

enum WeightType {
    UNDER_WEIGHT,
    NORMAL,
    OVER_WEIGHT,
    OBESE,
    DEFAULT
};

struct BodyData {
    double weight;
    double height;
    double chest_girth;
    double waist_girth;
    double hip_girth;
    double body_fat;
    double muscle;
    double water;
    double protein;
    double visceral_fat;
    double basal_metabolism;
    double bmi;
    WeightType weight_type;
};

#endif // BODYDATA_H
