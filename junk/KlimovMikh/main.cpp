#include "mainwindow.h"

#include <QApplication>
#include <QFile>
#include "persondatawidget.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PersonDataWidget widget;
    PersonData data({}, {});
//    MainWindow w;
//    w.setWidget(data);
//    w.show();
    widget.setPersonData(data);
    widget.show();
    return a.exec();
}
