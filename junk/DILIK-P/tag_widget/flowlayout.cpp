#include <QtWidgets>

#include "flowlayout.h"

FlowLayout::FlowLayout(QWidget *parent)
    : QLayout(parent)
{
    init();
}

FlowLayout::FlowLayout()
{
    init();
}


void FlowLayout::init(){
    QPushButton* qpb = new QPushButton(tr("\u25BC"));
    qpb->setStyleSheet("QPushButton {\
        color: #E0E0E0;\
        font-size: 25px;\
        background-color: #666666;\
        border-style: solid;\
        border-width:4px;\
        border-radius:17px;\
        border-color: #444444;\
        max-width:30px;\
        max-height:30px;\
        min-width:30px;\
        min-height:30px;\
    }\
\
    QPushButton:pressed {\
        color: #ECECEC;\
        font-size: 24px;\
        background-color: #818589;\
        border-color: #36454F;\
    }");
    connect(qpb, &QPushButton::clicked, this, &FlowLayout::turnFlag);
    addWidget(qpb);
    qpb->hide();
}


FlowLayout::~FlowLayout()
{
    QLayoutItem *item;
    while ((item = FlowLayout::takeAt(0)))
        delete item;
}

void FlowLayout::addItem(QLayoutItem *item)
{
    itemList.append(item);
}

int FlowLayout::horizontalSpacing() const
{
    return smartSpacing(QStyle::PM_LayoutHorizontalSpacing);
}

int FlowLayout::verticalSpacing() const
{

    return smartSpacing(QStyle::PM_LayoutVerticalSpacing);
}

int FlowLayout::count() const
{
    return itemList.size();
}

QLayoutItem *FlowLayout::itemAt(int index) const
{
    return itemList.value(index);
}

QLayoutItem *FlowLayout::takeAt(int index)
{
    if (index >= 0 && index < itemList.size())
        return itemList.takeAt(index);
    return nullptr;
}

Qt::Orientations FlowLayout::expandingDirections() const
{
    return { };
}

bool FlowLayout::hasHeightForWidth() const
{
    return true;
}

int FlowLayout::heightForWidth(int width) const
{
    int height = doLayout(QRect(0, 0, width, 0), true);
    return height;
}

void FlowLayout::setGeometry(const QRect &rect)
{
    QLayout::setGeometry(rect);
    doLayout(rect, false);
}

QSize FlowLayout::sizeHint() const
{
    return minimumSize();
}

QSize FlowLayout::minimumSize() const
{
    QSize size;
    for (const QLayoutItem *item : std::as_const(itemList))
        size = size.expandedTo(item->minimumSize());

    const QMargins margins = contentsMargins();
    size += QSize(margins.left() + margins.right(), margins.top() + margins.bottom());
    return size;
}

void FlowLayout::turnFlag(){
    closedFlag ^= 1;
    update();
}

int FlowLayout::doLayout(const QRect &rect, bool testOnly) const{
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QRect effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
    int x = effectiveRect.x();
    int y = effectiveRect.y();
    int lineHeight = 0;

    int i = 1;
    for (; i < itemList.size(); i++) {
        QLayoutItem* item = itemList[i];
        QWidget *wid = item->widget();
        wid->show();

        int spaceX = horizontalSpacing();
        int spaceY = verticalSpacing();

        int nextX = x + item->sizeHint().width() + spaceX;
        int w0 = 30; //костыль, иначе проблемы с переключением режимов

        if (i == itemList.size() - 1 && nextX - spaceX <= effectiveRect.right()){
            if (!testOnly){
                if (itemList[0]->widget()->isVisible())
                    itemList[0]->widget()->hide();
                item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
            }
            lineHeight = qMax(lineHeight, item->sizeHint().height());
            break;
        }

        if(y == effectiveRect.y() && nextX > effectiveRect.right() - w0){ //проверяем, что если нужна кнопка, то она дальше не влезет
            if (!testOnly){
                itemList[0]->widget()->show();
                itemList[0]->setGeometry(QRect(QPoint(effectiveRect.right() - w0, y), itemList[0]->sizeHint()));
            }

            x = effectiveRect.x();
            y = y + lineHeight + spaceY;
            nextX = x + item->sizeHint().width() + spaceX;
            lineHeight = 0;
            if (closedFlag){
                if (wid->isVisible()){
                    wid->hide();
                }
                break;
            }
        }

        if (nextX - spaceX > effectiveRect.right()) {
            x = effectiveRect.x();
            y = y + lineHeight + spaceY;
            nextX = x + item->sizeHint().width() + spaceX;
            lineHeight = 0;
        }

        if (!testOnly)
            item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));

        x = nextX;
        lineHeight = qMax(lineHeight, item->sizeHint().height());
    }

    if (!testOnly){
        for (; i < itemList.size(); i++) {
            QLayoutItem* item = itemList[i];
            QWidget *wid = item->widget();
            if (wid->isVisible()){
                wid->hide();
            }
        }
    }

    return y + lineHeight - rect.y() + bottom;
}


int FlowLayout::smartSpacing(QStyle::PixelMetric pm) const
{
    QObject *parent = this->parent();
    if (!parent) {
        return -1;
    } else if (parent->isWidgetType()) {
        QWidget *pw = static_cast<QWidget *>(parent);
        return pw->style()->pixelMetric(pm, nullptr, pw);
    } else {
        return static_cast<QLayout *>(parent)->spacing();
    }
}
