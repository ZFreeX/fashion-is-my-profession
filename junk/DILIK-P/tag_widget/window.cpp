#include <QPushButton>

#include "tagbar.h"
#include "window.h"
#include <random>

Window::Window()
{
    std::vector<std::pair<QString, int>> TagInfo1 = {
        {"Sales!", 0},
        {"Winter collection", 1},
        {"Eco-friendly", 2},
        {"Female", 3},
        {"Made in Chine", 4},
        {"Silver color", 5},
        {"Interesting text", 0},
        {"Smok Wid evrdy", 2},
        {"Chin chan chon chi", 4}};
    std::vector<std::pair<QString, int>> TagInfo2 = {
        {"Never gonna", 3},
        {"give you up", 4},
        {"Never", 0},
        {"gonna", 1}};

    DataBase db;
    db.connectToDataBase();
    db.clearAll();
    db.createTables();

    std::mt19937 rnd;
    //Example
    for (auto& tagInf: TagInfo1){
        int idTag = rnd();
        int conTag = rnd();
        db.addRowTags(idTag, tagInf.first, tagInf.second);
        db.addRowConnections(conTag, idTag, 1);
    }

    //Still example
    for (auto& tagInf: TagInfo2){
        int idTag = rnd();
        int conTag = rnd();
        db.addRowTags(idTag, tagInf.first, tagInf.second);
        db.addRowConnections(conTag, idTag, 2);
    }

    TagBar* tagbar1 = new TagBar(nullptr, db, 1);

    TagBar* tagbar2 = new TagBar(nullptr, db, 2);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(tagbar1);
    layout->addWidget(tagbar2);

    setLayout(layout);
    setFixedSize(1179*4/10, 2556/3/3);
    setStyleSheet("Window {background: #D3D3D3 }");
    setWindowTitle(tr("Tag Widget"));
}
