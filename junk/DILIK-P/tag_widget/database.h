#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QDebug>

#define DATABASE_NAME       "db.sqlite"

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = 0);
    ~DataBase();
    // connects to database
    bool connectToDataBase();  // 1
    // creates tables
    bool createTables();
    // clears table with given name
    bool clear(const QString& table);  // 1
    // clears and deletes all table within this database
    bool clearAll();  // 1
    // delets row by ID
    bool deleteRow(QString table, int id);  // 1
    // fills n rows with random values
    bool fillTableWithRandomData(const QString& tableName, int n);  // 1

    // tags table
    // adds row with ID=id, NAME=name, COLOR=color
    bool addRowTags(int id, QString name, int color);  // 1
    // adds multiple rows
    bool addRowsTags(std::list<int> ids, std::list<QString> names, std::list<int> colors);  // 1
    // get data by id
    std::pair<QString, int> getTagInfo(int id);
    bool deleteRow(QString name);  // 1

    // tags_clothes table
    // adds row with ID=id, ID_TAG=id_tag, ID_ITEM=id_item
    bool addRowConnections(int id, int id_tag, int id_item);  // 1
    // adds multiple rows
    bool addRowsConnections(std::list<int> ids, std::list<int> ids_tags, std::list<int> ids_items);  // 1
    // get all tags from item
    std::vector<int> getTags(int id);




private:
    QSqlDatabase    db;
};

#endif // DATABASE_H
