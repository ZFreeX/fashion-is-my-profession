//
// Created by Pavel Kasila on 7.05.24.
//

#ifndef CREATIVE_STATISTICS_EXPORT_VIEW_H
#define CREATIVE_STATISTICS_EXPORT_VIEW_H

#include "statistics_mode.h"

#include <QPushButton>
#include <QString>
#include <QVBoxLayout>
#include <QWidget>
#include <memory>

namespace outfit::core::statistics {
class StatisticsExportView : public QWidget {
   public:
    explicit StatisticsExportView(StatisticsMode mode);
    void setMode(StatisticsMode mode);

   private:
    std::unique_ptr<QVBoxLayout> layout_manager_;
    std::unique_ptr<QPushButton> export_button_;
    StatisticsMode mode_;
    QString countDataQuery();

    void exportCount();
};
}  // namespace outfit::core::statistics

#endif  // CREATIVE_STATISTICS_EXPORT_VIEW_H
