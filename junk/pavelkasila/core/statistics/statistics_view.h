//
// Created by Pavel Kasila on 7.05.24.
//

#ifndef CREATIVE_STATISTICS_VIEW_H
#define CREATIVE_STATISTICS_VIEW_H

#include "statistics_chart_view.h"
#include "statistics_export_view.h"
#include "statistics_mode.h"

#include <QComboBox>
#include <QString>
#include <QVBoxLayout>
#include <QWidget>
#include <memory>

namespace outfit::core::statistics {
class StatisticsView : public QWidget {
   public:
    StatisticsView();

   private:
    std::unique_ptr<QVBoxLayout> layout_manager_;
    std::unique_ptr<StatisticsChartView> chart_view_;
    std::unique_ptr<StatisticsExportView> export_view_;
    std::unique_ptr<QComboBox> mode_selector_;
    StatisticsMode mode_{StatisticsMode::Categories};

    void setMode(int index);
};
}  // namespace outfit::core::statistics

#endif  // CREATIVE_STATISTICS_VIEW_H
