#include "racoon.h"

QRectF Racoon::boundingRect() const
{
    return QRectF(0, 0, 500, 500);
}

void Racoon::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawImage(QRectF(0, 0, 400, 300), racoon);
}

Racoon::Racoon() {}
