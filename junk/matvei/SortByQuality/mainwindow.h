#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#pragma once

#include <algorithm>

#include <QDebug>
#include <QGraphicsScene>
#include <QMainWindow>
#include <QMessageBox>
#include <QString>
#include <QVector>

//#include "racoon.h"
#include "outfit.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    //Racoon* myRacoon;
    //QGraphicsScene* scene;

    void sortByParameter();
    static bool comp(const Outfit& dressOne, const Outfit& dressTwo);
};
#endif // MAINWINDOW_H
