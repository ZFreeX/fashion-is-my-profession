#include "form.h"
#include "eventbase.h"
#include "ui_form.h"

Form::Form(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Form)
{
    ui->setupUi(this);

}

Form::~Form()
{
    delete ui;
}

void Form::on_pushButton_clicked()
{
    bool i = ui->indoorcheck->isChecked();
    QTime d = ui->duration->time();
    QDateTime dt = QDateTime::currentDateTime();
    std::string mood = ui->eventbox->currentText().toStdString();
    std::string weather = ui->weatherbox->currentText().toStdString();
    int t = static_cast<int>(ui->temperaturebox->value());
    std::string officiality = ui->officialbox->currentText().toStdString();
    Mood m = mood_map[mood];
    Weather w = weather_map[weather];
    Officiality o = officiality_map[officiality];
    Event event(o, m, w, i, t, d, dt);

}
